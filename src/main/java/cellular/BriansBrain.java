package cellular;
import java.util.Random;
import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton{


	int rows;
	int columns;

	/**
	 * The grid of cells
	 */
	IGrid currentGeneration;

	/**
	 * 
	 * Construct a Game Of Life Cell Automaton that holds cells in a grid of the
	 * provided size
	 * 
	 * @param height The height of the grid of cells
	 * @param width  The width of the grid of cells
	 */
	public BriansBrain(int rows, int columns) {
		this.rows = rows;
		this.columns = columns;
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

    @Override
    public CellState getCellState(int row, int col) {
        // TODO
		CellState state = currentGeneration.get(row, col);
		return state;
    }

    @Override
    public void initializeCells() {
        // TODO Auto-generated method stub
        Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
        
    }

    @Override
	public void step() {
		// TODO
		for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
				currentGeneration.set(i, j, getNextCell(i,j));
			}
		}
	}

    @Override
	public CellState getNextCell(int row, int col) {
		// TODO

		CellState state = getCellState(row, col);	

		if (state == CellState.ALIVE ){
			return CellState.DYING; 
		}

		else if (state.equals(CellState.DYING) ){ 
			return CellState.DEAD;
		}
		else if (state.equals(CellState.DEAD) && countNeighbors(row, col, state) == 2){
			return CellState.ALIVE;

		}
        else if (state.equals(CellState.DEAD)){
			return CellState.DEAD;

		}
		else {
			return state;
		}
	}

    @Override
    public int numberOfRows() {
        // TODO Auto-generated method stub
        return rows;
    }

    @Override
    public int numberOfColumns() {
        // TODO Auto-generated method stub
        return columns;
    }

    @Override
	public IGrid getGrid() {
		return currentGeneration;
	}
    
    private int countNeighbors(int row, int col, CellState state) {
		// TODO
		int count = 0;
		int boarderXleft = 1;
		int boarderXright = 1;
		int boarderYtop = 1;
		int boarderYbot = 1;
		if (state.equals(CellState.ALIVE)) {
			count --;
		}
		if (row == 0){
			boarderYtop --;
		}
		if (numberOfRows() - row-1 == 0){
			boarderYbot --;
		}
		if (col == 0){
			boarderXleft --;
		}
		if (numberOfColumns() - col-1 == 0){
			boarderXright --;
		}
		for (int i = row-boarderYtop; i < row+(boarderYbot+1); i++) {
            for (int j = col-boarderXleft; j < col+(boarderXright+1); j++) {
				if (getCellState(i,j).equals(CellState.ALIVE)){
					count ++;
				}
			}
		}
		return count;
	}
}
