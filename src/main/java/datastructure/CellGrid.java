package datastructure;
import cellular.CellState;

public class CellGrid implements IGrid {

    int columns;
    int rows;
    CellState[][] cellstate;
    private CellState intialState;

    
    public CellGrid(int rows, int columns, CellState initialState) { 
		// TODO Auto-generated constructor stub
        this.columns = columns;
        this.rows = rows;
        this.intialState = initialState;
        this.cellstate = new CellState[rows][columns];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                cellstate[i][j] = initialState;
            }
        }

	}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
        if (row < numRows() && row >= 0 && column < numColumns() && column >= 0) {
            cellstate[row][column] = element;
        } 
        else{
            throw new IndexOutOfBoundsException();
        }

         
    }

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
        if (row < numRows() && row >= 0 && column < numColumns() && column >= 0) {
            return cellstate[row][column];
        }
        else{
            throw new IndexOutOfBoundsException();
            //return null;
        }
    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
        IGrid newGrid = new CellGrid(numRows(), numColumns(), intialState);
        for (int i = 0; i < numRows(); i++) {
			for (int j = 0; j < numColumns(); j++) {
				newGrid.set(i, j, cellstate[i][j]);
			}
		}
        return newGrid;
    }
    
}
